package com.hoteladvisors.contact.web;

import lombok.Data;

@Data
public class AddressView {
    private long id;
    private String city;
    private String street;
    private String house;
    private String room;
    private String postIndex;
}
