package com.hoteladvisors.contact.web;

import com.hoteladvisors.contact.persistence.Gender;
import com.hoteladvisors.contact.service.PersonService;
import lombok.Getter;
import lombok.Setter;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.*;

import java.util.ArrayList;

@VariableResolver(org.zkoss.zkplus.cdi.DelegatingVariableResolver.class)
public class RootComposer extends SelectorComposer<Component> {

    @WireVariable("personService")
    private PersonService personService;

    @Wire("#surname-input")
    private Textbox surnameTextbox;
    @Wire("#name-input")
    private Textbox nameTextbox;
    @Wire("#patronymic-input")
    private Textbox patronymicTextbox;
    @Wire("#phone-number-input")
    private Textbox phoneNumberTextbox;
    @Wire("#gender-radio")
    private Radiogroup genderRadio;
    @Wire("#male-radio")
    private Radio maleRadio;

    @Wire("#add-address-form")
    private Grid addAddressForm;
    @Wire("#city-input")
    private Textbox cityTextbox;
    @Wire("#street-input")
    private Textbox streetTextbox;
    @Wire("#house-input")
    private Textbox houseTextbox;
    @Wire("#room-input")
    private Textbox roomTextbox;
    @Wire("#post-index-input")
    private Textbox postIndexTextbox;

    @Wire("#personTable")
    private Listbox personTable;

    @Wire("#addressTable")
    private Listbox addressTable;

    @Getter
    @Setter
    private ListModelList<PersonView> persons = new ListModelList<>();

    @Getter
    @Setter
    private ListModelList<AddressView> addresses = new ListModelList<>();

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        persons.addAll(personService.findAll());
        personTable.addEventListener("onDelete", this::onDeletePerson);
        addressTable.addEventListener("onDelete", this::onDeleteAddress);
    }

    @Listen("onClick = #add-person-button")
    public void addNewPerson(){
        String surname = getRequiredString(surnameTextbox);
        String name = getRequiredString(nameTextbox);
        String patronymic = patronymicTextbox.getValue();
        String phoneNumber = getRequiredString(phoneNumberTextbox);
        String gender = genderRadio.getSelectedItem().getValue();

        if (hasAnyNull(surname, name, phoneNumber)) {
            return;
        }

        PersonView person = new PersonView();
        person.setSurname(surname);
        person.setName(name);
        person.setPatronymic(patronymic);
        person.setPhoneNumber(phoneNumber);
        person.setGender(Gender.valueOf(gender));
        person.setAddresses(new ArrayList<>());

        person = personService.create(person);
        persons.add(person);
        clearForms();
    }

    @Listen("onClick = #add-address-button")
    public void addNewAddress() {
        PersonView person = getCurrentSelectedPerson();

        String city = getRequiredString(cityTextbox);
        String street = getRequiredString(streetTextbox);
        String house = getRequiredString(houseTextbox);
        String room = roomTextbox.getValue();
        String postIndex = postIndexTextbox.getValue();

        if (hasAnyNull(city, street, house)) {
            return;
        }

        AddressView address = new AddressView();
        address.setCity(city);
        address.setStreet(street);
        address.setHouse(house);
        address.setRoom(room);
        address.setPostIndex(postIndex);

        person.getAddresses().add(address);
        personService.update(person);
        addresses.add(address);
        clearForms();
    }

    private void clearForms() {
        cityTextbox.setValue("");
        streetTextbox.setValue("");
        houseTextbox.setValue("");
        roomTextbox.setValue("");
        postIndexTextbox.setValue("");
        surnameTextbox.setValue("");
        nameTextbox.setValue("");
        patronymicTextbox.setValue("");
        phoneNumberTextbox.setValue("");
        genderRadio.setSelectedItem(maleRadio);
    }

    @Listen("onSelect = #personTable")
    public void personSelected() {
        addresses.clear();
        addresses.addAll(getCurrentSelectedPerson().getAddresses());
        setAddressVisible(true);
    }

    public void onDeletePerson(Event event) {
        PersonView person = (PersonView) event.getData();
        personService.delete(person);
        persons.remove(person);
        if (personTable.getSelectedItem() == null) {
            setAddressVisible(false);
        }
    }

    public void onDeleteAddress(Event event) {
        AddressView address = (AddressView) event.getData();
        PersonView person = getCurrentSelectedPerson();
        person.getAddresses().remove(address);
        personService.update(person);
        addresses.remove(address);
    }

    private void setAddressVisible(boolean visible) {
        addAddressForm.setVisible(visible);
        addressTable.setVisible(visible);
    }

    private PersonView getCurrentSelectedPerson() {
        Listitem selectedItem = personTable.getSelectedItem();
        if (selectedItem == null) {
            throw new RuntimeException("Person is not selected");
        }
        return selectedItem.getValue();
    }

    private String getRequiredString(Textbox textbox) {
        String value = textbox.getValue();
        if (value == null || value.isEmpty()) {
            textbox.setErrorMessage("This field is required");
            return null;
        } else {
            textbox.clearErrorMessage();
            return value;
        }
    }

    private boolean hasAnyNull(Object... objects) {
        for (Object o : objects) {
            if (o == null) {
                return true;
            }
        }
        return false;
    }

}
