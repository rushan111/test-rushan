package com.hoteladvisors.contact.web;

import com.hoteladvisors.contact.persistence.Gender;
import lombok.Data;

import java.util.List;

@Data
public class PersonView {
    private long id;
    private String  surname;
    private String  name;
    private String  patronymic;
    private String  phoneNumber;
    private Gender gender;
    private List<AddressView> addresses;
}
