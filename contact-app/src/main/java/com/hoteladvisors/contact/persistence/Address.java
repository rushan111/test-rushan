package com.hoteladvisors.contact.persistence;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "address")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String city;

    @Column(nullable = false)
    private String street;

    @Column(nullable = false)
    private String house;

    private String room;

    @Column(name = "post_index")
    private String postIndex;

    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person person;
}
