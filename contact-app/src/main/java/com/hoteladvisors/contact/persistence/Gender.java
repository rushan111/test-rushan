package com.hoteladvisors.contact.persistence;

public enum Gender {
    MALE,
    FEMALE
}
