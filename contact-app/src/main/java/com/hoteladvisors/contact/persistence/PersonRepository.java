package com.hoteladvisors.contact.persistence;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class PersonRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Person addNew(Person p) {
        entityManager.persist(p);
        return p;
    }

    public List<Person> findAll() {
        return entityManager.createQuery("select p from Person p", Person.class).getResultList();
    }

    public Person findOne(long id) {
        return entityManager.find(Person.class, id);
    }

    public void delete(long id) {
        Person person = entityManager.find(Person.class, id);
        entityManager.remove(person);
    }

    public void update(Person person) {
        entityManager.merge(person);
    }
}
