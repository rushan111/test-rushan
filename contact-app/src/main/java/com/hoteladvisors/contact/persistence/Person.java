package com.hoteladvisors.contact.persistence;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "person")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String  surname;

    @Column(nullable = false)
    private String  name;

    private String  patronymic;

    @Column(name = "phone_number", nullable = false)
    private String phoneHumber;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "person", orphanRemoval = true)
    private List<Address> addresses;
}
