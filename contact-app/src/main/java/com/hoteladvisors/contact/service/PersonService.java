package com.hoteladvisors.contact.service;

import com.hoteladvisors.contact.persistence.Address;
import com.hoteladvisors.contact.persistence.Person;
import com.hoteladvisors.contact.persistence.PersonRepository;
import com.hoteladvisors.contact.web.AddressView;
import com.hoteladvisors.contact.web.PersonView;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@Named
public class PersonService {

    @Inject
    private PersonRepository personRepository;

    public List<PersonView> findAll() {
        return personRepository.findAll().stream().map(this::convertToView).collect(Collectors.toList());
    }

    public PersonView create(PersonView personView) {
        Person p = new Person();
        p.setSurname(personView.getSurname());
        p.setName(personView.getName());
        p.setPatronymic(personView.getPatronymic());
        p.setPhoneHumber(personView.getPhoneNumber());
        p.setGender(personView.getGender());
        p.setAddresses(new ArrayList<>());
        return convertToView(personRepository.addNew(p));
    }

    public void update(PersonView personView) {
        Person person = personRepository.findOne(personView.getId());
        person.setSurname(personView.getSurname());
        person.setName(personView.getName());
        person.setPatronymic(personView.getPatronymic());
        person.setPhoneHumber(personView.getPhoneNumber());
        person.setGender(personView.getGender());
        List<Address> addresses = personView.getAddresses().stream()
                .map(a -> convertToEntity(person, a))
                .collect(Collectors.toList());
        person.getAddresses().clear();
        person.getAddresses().addAll(addresses);
        personRepository.update(person);
    }

    public void delete(PersonView pv) {
        personRepository.delete(pv.getId());
    }

    private PersonView convertToView(Person person) {
        PersonView pv = new PersonView();
        pv.setId(person.getId());
        pv.setSurname(person.getSurname());
        pv.setName(person.getName());
        pv.setPatronymic(person.getPatronymic());
        pv.setPhoneNumber(person.getPhoneHumber());
        pv.setGender(person.getGender());
        pv.setAddresses(person.getAddresses().stream().map(this::convertToView).collect(Collectors.toList()));
        return pv;
    }

    private AddressView convertToView(Address address) {
        AddressView av = new AddressView();
        av.setCity(address.getCity());
        av.setStreet(address.getStreet());
        av.setHouse(address.getHouse());
        av.setRoom(address.getRoom());
        av.setPostIndex(address.getPostIndex());
        return av;
    }

    private Address convertToEntity(Person person, AddressView addressView) {
        Address address = new Address();
        address.setId(addressView.getId());
        address.setCity(addressView.getCity());
        address.setStreet(addressView.getStreet());
        address.setHouse(addressView.getHouse());
        address.setRoom(addressView.getRoom());
        address.setPostIndex(addressView.getPostIndex());
        address.setPerson(person);
        return address;
    }
}
