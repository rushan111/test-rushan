# rushan-test

## Требования
- база данных MySQL
- java 11

## Как запустить


- Скачать wildfly 10 с официального сайта


- Скачать mysql java connector (https://repo1.maven.org/maven2/mysql/mysql-connector-java/8.0.28/mysql-connector-java-8.0.28.jar)


- Запустить wildfly, зайти в wildfly-CLI и выполнить следующие команды:


В первой строчке бязательно заменить путь к mysql-connector-java на тот, куда вы скачали.
Во второй строчке заменить имя пользователя и пароль к базе данных на актуальные.


```
deploy ~/Downloads/mysql-connector-java-8.0.28.jar

data-source add --name=contact-ds --jndi-name=java:/jdbc/contact-ds --driver-name=mysql-connector-java-8.0.28.jar --connection-url=jdbc:mysql://127.0.0.1:3306/contact?createDatabaseIfNotExist=true --user-name=root --password=root
```


- Склонировать этот проект, из корня проекта выполнить
 ```
 ./gradlew ear
 ```

- Скопировать файл `rushan-test-1.ear` из папки `build/libs` в папку `$WILDFLY_HOME/standalone/deployments`

- Открыть в браузере `http://localhost:8080/contact-app-1`

Всё.
